@extends('Master.master')
@section('current_page_name', 'Product Category'); 
@section('content') 


    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <style>
            th,td{
                text-align:center;
            }
   
    </style>

             <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Product Category</h3>
                        </div>
                        <!-- /.card-header --> 
                        <div class="card-body">

                            
                            <div class="row py-2" id="hide1">
                                    <div class="col-4">
                                    <a href="#" class="btn btn-sm btn-primary btn-content AddNew"  data-toggle="modal" data-target="#exampleModal" id="AddNew" style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                                    </div>
                                    <div class="col-8 my-auto mr-auto">
                                        <form class="form-inline mr-auto float-right">
                                            <input class="form-control mr-sm-2 enter_search" type="text" id="search" placeholder="Search Supplire" aria-label="Search Supplire">
                                            <button class="btn btn-cyan btn-rounded my-0"
                                                    id="txtUserSearh" 
                                                    type="submit">
                                                <i class="fas fa-search btn_search"></i>
                                            </button>
                                        </form>
                                    </div>
                            </div>

                            <table class="table table-bordered">
                                <thead>                  
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Icon</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="pro_content">
                                                
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right" id="pagination-list">
                                <!-- page list here -->
                            </ul>
                        </div>
                        </div>
                    
            
                    </div>
                    
                    </div>
            
                </div>
            </section>



            <!-- ====== Modal Form ====== -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-center">Create Product Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="md-form mb-3">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" class="form-control" id="name"  name="name" value="">
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>
                            <div class="md-form mb-3">
                                <i class="fas fa-key prefix grey-text"></i>
                                <input type="text" class="form-control" id="icon" name="icon" value="">
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>

                            <div class="md-form mb-3">
                                <i class="fas fa-doc prefix grey-text"></i>
                                <textarea name="desc" id="desc" class="editor"></textarea>
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;">Close</button>
                            <button type="button" onclick="add_pro_cat(this)" class="btn btn-primary btn-sm  btn-content btn-content-add"  style="border-radius: 3.2rem;" data-dismiss="modal">Save</button>
                        </div>


                    </div>
                </div>
            </div>


            <!-- ====== Modal Form  Edit ====== -->

            <div class="modal fade hidden" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-center">Edit Product Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="md-form mb-3">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" class="form-control" id="e_name"  name="e_name" value="">
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>
                            <div class="md-form mb-3">
                                <i class="fas fa-key prefix grey-text"></i>
                                <input type="text" class="form-control" id="e_icon" name="e_icon" value="">
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>

                            <div class="md-form mb-3">
                                <i class="fas fa-doc prefix grey-text"></i>
                                <textarea name="e_desc" id="e_desc" class="editor"></textarea>
                                <div class="firstname-valid-feedback">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;">Close</button>
                            <button type="button"  class="btn btn-primary btn-sm" id="btn_submit" style="border-radius: 3.2rem;" data-dismiss="modal">Update</button>
                        </div>


                    </div>
                </div>
            </div>



        <script type="text/javascript">

        var totalPage = 0;
        var currentPage = 1;
        var perPage = 2;

        $(document).ready(function () {
            fetch_data();
        });

        function fetch_data() {
            $("#pro_content").html("");
            let params = {
                page: currentPage,
                size: perPage
            }
            $.ajax({
                headers: HEADER,
                url: BASE_URL + "product_cate",
                method: "GET",
                data: params,
                success:function(res){

                    console.log("pro_cate:",res)
                    var contents = "";
                    $.each(res.data,function(index,val){
                        contents = '<tr>'
                                        +'<td class="id">'+ val.id+'</td>'
                                        +'<td>'+ val.name+'</td>'
                                        +'<td>'+ val.icon+'</td>'
                                        +'<td>'+ val.description+'</td>'
                                        +'<td>'
                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                                        + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                                        +'</td>'
                                    + '</tr>';                      
                        $("#pro_content").append(contents);
                    });
                        //PAGINATION
                        currentPage = res.current_page;
                        totalPage = res.total_record/perPage;
                        renderPagination(totalPage);
                },
                error: function(e){
                    console.log(e)
                }
            });
        }
        $("document").on("click","#AddNew",function(){
        });
 
        // Add Product Category
        function add_pro_cat(click) {
            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+"product_cate",
                method: "POST",
                data: {
                    name : $("#name").val(),
                    icon : $("#icon").val(),
                    description : $("#desc").val()
                },
                success:function(res){
                    
                    $("#name").val('');
                    $("#icon").val('');
                    $("#desc").val('');
                    $("#pro_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log("this is error:",e)
                }
            })
        }
        // show modal edit
        $("body").on("click",".btn_edit",function(){

            let item = $(this).attr("data-item");
            const obj = JSON.parse(decodeURIComponent(item));
           
            $("#btn_submit").attr("data-id", obj.id);
            $("#e_name").val(obj.name);
            $("#e_icon").val(obj.icon);
            $("#e_desc").val(obj.description);
            $('#ModalEdit').modal('show');
        });
        // click Update Button
        $("body").on("click",'#btn_submit',function () {
            let id = $(this).attr("data-id");
            edit_pro_cate(id);
        })
        // Edit Action
        function edit_pro_cate(id){
            var param = {
                name : $("#e_name").val(),
                icon : $("#e_icon").val(),
                description : $("#e_desc").val()
            };

            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'product_cate/' + id,
                method: "PUT",
                data: param,

                success:function(res){
                    $('#ModalEdit').modal('hide');
                    $("#pro_content").html('');
                    fetch_data();
                },
                error: function(e){
                    swal("bad job", "You clicked the button!", "error");
                    console.log(e.responseText)
                }
            })
        }

        //========= Event Delete with Sweetalert ===========//

        $(document).on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_pro_cate(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
        });



        function delete_pro_cate(id){

            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'product_cate/' + id,

                method: "DELETE",

                success:function(res){

                    $('#ModalEdit').modal('hide');
                    $("#pro_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
        }

     </script>

     <!-- PAGINATION RENDER -->
    <script>
        function renderPagination(totalPage) {
            $('#pagination-list').html("");
            let prev = '<li class="page-item" id="btn-prev"><a class="page-link" href="#">&laquo;</a></li>';
            $('#pagination-list').append(prev);
            for (let i = 0; i < totalPage; i++) {
                if ((i+1) == currentPage) {
                    let html = '<li class="page-item active pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }  else {
                    let html = '<li class="page-item pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }

            }
            let next = '<li class="page-item"><a class="page-link" href="#" id="btn-next">&raquo;</a></li>';
            $('#pagination-list').append(next);
        }

        $('body').on('click', '.pagination-item', function () {
            let current_page = $(this).find('a').html();
            currentPage = parseInt(current_page);
            fetch_data();
        })

        $('body').on('click', '#btn-prev', function () {
            currentPage --;
            fetch_data();
        })

        $('body').on('click', '#btn-next', function () {
            currentPage ++;
            fetch_data();
        })

    </script>

@endsection
