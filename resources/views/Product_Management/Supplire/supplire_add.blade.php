
@extends('Master.master')
@section('current_page_name', 'Supplire Create');
@section('content')



    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

       <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <style>

        .supplire_view .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }
        .supplire_add .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }
        

    </style>

        <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">

        <div class="col-md-1"></div>
         
         <div class="col-md-10">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Full Name</label>
                    <input type="text" class="form-control" id="f_name"  name="f_name" value="" placeholder="Enter FullName">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Short Name</label>
                    <input type="text" class="form-control" id="s_name"  name="s_name" value="" placeholder="Enter ShortName">
                  </div>

              
                  <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="email" class="form-control" id="email"  name="email" value="" placeholder="Enter email">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Phone</label>
                    <input type="phone" class="form-control" id="phone"  name="phone" value="" placeholder="Enter phone">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Website</label>
                    <input type="text" class="form-control" id="website"  name="website" value="" placeholder="Enter website">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Address</label>
                    <input type="text" class="form-control" id="address"  name="address" value="" placeholder="Enter Address">
                  </div>

                   <!--  Licence -->
               <!--    <div class="form-group">
                    <label for="exampleInputFile">File input License</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="license">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div> -->

                  <div class="form-group">
                      <label for="exampleInputPassword1">Licence Profile</label>
                      <input type="text" class="form-control" id="license">
                  </div>

                   <!--  Description -->
                    <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                            <h3 class="card-title">
                                Description Detail of Supplire
                                
                            </h3>
                            <!-- tools box -->
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                                        title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body pad">
                            <div class="mb-3">
                                <textarea class="textarea" placeholder="Place some text here" id="description" 
                                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                            <!-- <p class="text-sm mb-0">
                                Editor <a href="#">Documentation and license
                                information.</a>
                            </p> -->
                            </div>
                        </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- ./row -->
                    </section>
                    <!-- /.content -->

                <div class="card-footer">
                  <button  type="button" class="btn btn-primary float-right" id="btn_save">Submit</button>
                  <button  type="button" class="btn btn-danger float-right" id="btn_back">Cancal</button> 
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
        
        <div class="col-md-1"></div> 

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <script type="text/javascript">
    	
    	// Add Product Category

       $('#btn_save').click(function (){

        params = {
            FullName : $("#f_name").val(),
            ShortName : $("#s_name").val(),
            email : $("#email").val(),
            phone :  $("#phone").val(), 
            address : $("#address").val(),
            logo :    $("#logo").val(),
            website : $("#website").val(),
            License : $("#license").val(),
            description : $("#description").val()  
        }
        if(params.FullName == null || params.FullName   == ""){
          shows_error("Please Input FullName !!");
        }else if(params.ShortName == null || params.ShortName   == ""){
          shows_error("Please Input ShortName !!");
        }else if(params.phone == null || params.phone   == ""){
          shows_error("Please Input Phone Number !!");
        }else if(params.email == null || params.email   == ""){
          shows_error("Please Input Email !!");
        }else{

          $.ajax({
                headers: HEADER,
                url: BASE_URL+"supplire",
                method: "POST",
                data: params,

                success:function(res){

                	console.log("supplire: ",res.data);

                     //console.log("Data :",res.data);
                    $("#f_name").val('');
                    $("#s_name").val('');
                    $("#email").val('');
                    $("#phone").val('');
                    $("#address").val('');
                    $("#logo").val('');
                    $("#website").val('');
                    $("#license").val('');
                    $("#description").val('');

                   window.location.href = "{{url('supplire')}}";

                },
                error: function(e){
                    console.log("this is error:",e)
                }
            })
        }
           
       })

       $("#btn_back").click(function(){
          window.location.href = "{{url('supplire')}}";
       });

        // function validate
        function shows_error(errorMessage){
            swal("Error",errorMessage,"error");
        }

        	
        

    </script>



























@endsection