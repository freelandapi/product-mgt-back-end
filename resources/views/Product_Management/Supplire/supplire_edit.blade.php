
@extends('Master.master')
@section('current_page_name', 'Supplire Update');
@section('content')



    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <style>

        .supplire_view .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }
        .supplire_add .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }
        

    </style>

        <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">

        <div class="col-md-1"></div>
         
         <div class="col-md-10">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Supplire</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Full Name</label>
                    <input type="text" class="form-control" id="f_name"  name="f_name" value="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Short Name</label>
                    <input type="text" class="form-control" id="s_name"  name="s_name" value="">
                  </div>

              
                  <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="email" class="form-control" id="email"  name="email" value="">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Phone</label>
                    <input type="phone" class="form-control" id="phone"  name="phone" value="">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Website</label>
                    <input type="text" class="form-control" id="website"  name="website" value="">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Address</label>
                    <input type="text" class="form-control" id="address"  name="address" value="" >
                  </div>

                   <!--  Licence -->
               <!--    <div class="form-group">
                    <label for="exampleInputFile">File input License</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="license">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div> -->

                  <div class="form-group">
                     
                      <img src="" id="license" style="width:150px;height:150px;">
                     
                  </div>

                   <!--  Description -->
                    <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                            <h3 class="card-title">
                                Description Detail of Supplire
                                
                            </h3>
                            <!-- tools box -->
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                                        title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body pad">
                            <div class="mb-3">
                                <textarea class="textarea" placeholder="Place some text here" id="description" 
                                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                            <!-- <p class="text-sm mb-0">
                                Editor <a href="#">Documentation and license
                                information.</a>
                            </p> -->
                            </div>
                        </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- ./row -->
                    </section>
                    <!-- /.content -->

                <div class="card-footer">
                  <button  type="button" class="btn btn-primary float-right btn_save" id="btn_save">Update</button>
                  <button  type="button" class="btn btn-danger float-right" id="btn_back">Cancal</button> 
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
        
        <div class="col-md-1"></div> 

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <script type="text/javascript">
    	
    	// Add Product Category
 
      $("document").ready(function(){


        var url_string = window.location.href ;
        var url = new URL(url_string);
        var license_file = url.searchParams.get("License");

        if(license_file == "" || license_file == null){
          $("#license").hide();
        }else{
          $("#license").attr('src',license_file);
        }


        $("#f_name").val(url.searchParams.get("full_name"));
        $("#s_name").val(url.searchParams.get("short_name"));   
        $("#email").val(url.searchParams.get("email"));
        $("#phone").val(url.searchParams.get("phone"));
        $("#address").val(url.searchParams.get("address"));
        $("#logo").val(url.searchParams.get("logo"));
        //$("#license").attr('src',license_file);
        $("#website").val(url.searchParams.get("website"));
        $("#description").val(url.searchParams.get("description"));
    

        console.log(url.searchParams.get("email"));

      });
        // show modal edit
        $("body").on("click",".btn_save",function(){

          var url_string = window.location.href ;
          var url = new URL(url_string);
          var id = url.searchParams.get("id");
         

      //  console.log("id:",url.searchParams.get("id"));
          var params = {
                    FullName : $("#f_name").val(),
                    ShortName : $("#s_name").val(),
                    email : $("#email").val(),
                    phone : $("#phone").val(),
                    address : $("#address").val(),
                    logo : $("#logo").val(),
                    License : $("#license").val(),
                    website : $("#website").val(),
                    description : $("#description").val()
                   
                };

          console.log("params:",params);

          $.ajax({
                headers: HEADER,
                url: BASE_URL+'supplire/' + id,
                method: "PUT",
                data : params ,

                success:function(res){

                     window.location.href = "{{url('supplire')}}";
                    
                },
                error: function(e){
                    console.log(e)
                }
            })    
        });

        $("#btn_back").click(function(){
          window.location.href = "{{url('supplire')}}";
       });

    </script>



























@endsection