
@extends('Master.master')
@section('current_page_name', 'Supplire List');
@section('content')

    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->
    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



    <style>

        .supplire_view .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }
        .supplire_add .modal-dialog{
            max-width: 1000px;
            width: 100%;
        }

        th,td{
            text-align:center;
        }
        

    </style>

     
        <!-- Main content -->
        <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Supplire List</h3>
                </div>

                <div class="card-body">                 
                    <div class="row py-2" id="hide1">
                            <div class="col-4">
                            <a href="{{url('supplire_add')}}" class="btn btn-sm btn-primary btn-content"  style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                            </div>
                            <div class="col-8 my-auto mr-auto">
                                <form class="form-inline mr-auto float-right">
                                    <input class="form-control mr-sm-2 enter_search" type="text" id="search" placeholder="Search Supplire" aria-label="Search Supplire">
                                    <button class="btn btn-cyan btn-rounded my-0"
                                            id="txtUserSearh" 
                                            type="submit">
                                        <i class="fas fa-search btn_search"></i>
                                    </button>
                                </form>
                            </div>
                    </div>

                    
            
                    <table class="table table-bordered">
                    <thead>                  
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>FullName</th>
                        <th>ShortName</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Website</th>
                        <th>Licence</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="supplire_content">

                    </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm m-0 float-right" id="pagination-list">
                        <!-- page list here -->
                    </ul>
                </div>
                </div>
            
    
            </div>
            
            </div>
    
        </div>
        </section>
    <!-- /.content-wrapper -->

 


     <!-- ====== Modal Form  Show Detail ======  -->

     <div class="modal fade hidden supplire_view" id="ModalShow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        
        <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                  <!--  <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a> -->
                   <h3 class="modal-title">Show Detail</h3>
                   <button type="button" class="btn btn-tool" data-dismiss="modal" data-card-widget="remove"><i class="fas fa-times"></i></button>
                  
               </div>
               <div class="modal-body">
                   <div class="row">
                       <div class="col-md-5">
                           <img src="" class="img-responsive" id="license_img" style="width:100%;height:auto;">
                       </div>
                       <div class="col-md-7">
                           <p><b>Full Name:  </b> <span id="fullname"></span></p>
                           <p><b>NICK Name:  </b> <span id="shortname"></span></p>
                           <p><b>TEL:  </b><span id="phone"></span></p>
                           <p><b>WEBSITE:  </b><span id="website"></span></p>
                           <p><b>ADDRESS :  </b><span id="address"></span></p>
                           <!-- <p><b>LICENCE :  </b><span id="license"></span></p> -->
                           
                          
                           <p id="description"></p>
                         
                           
                       </div>
                   </div>
               </div>
           </div>
       </div>
    
   </div>
   <!-- ==== End Modal Show ==== -->


    <script>

    var totalPage = 0;
    var currentPage = 1;
    var perPage = 2;
     $(document).ready(function () { 
             fetch_data();
      });
      // FUNCTION FETCH DATA
        function fetch_data() {
            $("#supplire_content").html("");
            let params = {
                page: currentPage,
                size: perPage
            }
            $.ajax({           
                headers: HEADER,
                url: BASE_URL + "supplire",
                method: "GET",
                data : params,

                success:function(res){
                  //  console.log("supplire:",res.data);
                    var supplire = "";
                     let full_url = RAW_BASE_URL + 'default_user.png';
                    $.each(res.data,function(index,val){

                        supplire = '<tr>'
                                        +'<td class="id">'+ val.id+'</td>'
                                        +'<td>'+ val.FullName+'</td>'
                                        +'<td>'+ val.ShortName+'</td>'
                                        +'<td>'+ val.email+'</td>'
                                        +'<td>'+ val.phone+'</td>'
                                        +'<td>'+ val.website+'</td>'
                                        +'<td class="license_list">'
                                        +   '<img src="'+(val.License?val.License:full_url) +'" style="width:50px;height:50px;">'
                                        +'</td>'
                                        +'<td>'

                                        +'<button type="button"  style="border-radius: 20px;" class="btn btn-primary btn-sm btn_show" data-img="'+(val.License?val.License:full_url)+'" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-eye"></i></button>'

                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" data-id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                                        + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                                        +'</td>'
                                    + '</tr>';

                        $("#supplire_content").append(supplire); 
                    });
                        //PAGINATION
                        currentPage = res.current_page;
                        totalPage = res.total_record/perPage;
                        renderPagination(totalPage);
                },
                error: function(e){ 
                    console.log(e)
                } 
            });
    }
 
     let full_url = RAW_BASE_URL +'storage/' + 'default_user.png';

    $("body").on("click","#add_new",function(){ 

        $('#AddBrand').modal('show');
    });
 
    // Edit
    $("body").on("click",".btn_edit",function(){


        let id = $(this).attr("data-id");
        let data_item = $(this).attr("data-item");


        let data_edit = JSON.parse(decodeURIComponent(data_item));


        

        
        let full_name = data_edit.FullName;
        let short_name = data_edit.ShortName;
        let email = data_edit.email;
        let phone = data_edit.phone ; 
        let address = data_edit.address;
        let logo = data_edit.logo;
        let License = data_edit.License;
        let website = data_edit.website;
        let description = data_edit.description;


        let params = '?id='+id+'&full_name=' + full_name + '&short_name=' + short_name + '&email=' + email + '&phone=' + phone + '&address=' + address+'&website='+ website +'&description='+ description+'&logo=' + logo + '&License=' + License ;

        //var encodedParam = encodeURIComponent(params);                 

    
        window.location.href = "{{url('supplire_edit')}}" + params ;
        


    });

    
   



    // Show Detail 
    $("body").on("click",".btn_show",function(){


        let item = $(this).attr("data-item");
        const obj = JSON.parse(decodeURIComponent(item));
       // console.log("data:",obj)


        $("#fullname").text(obj.FullName);
        $("#shortname").text(obj.ShortName);
        $("#address").text(obj.address);
        $("#phone").text(obj.phone);
        $("#website").text(obj.website);
        $("#description").text(obj.description);
       // $("#license").text(obj.License);
        let img = $(this).attr("data-img");
       // console.log("img:",img);
        $("#license_img").attr('src',img);


        $('#ModalShow').modal('show');
    });


    // Delete 
     $(document).on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_supplire(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
        });



        function delete_supplire(id){

            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'supplire/' + id,

                method: "DELETE",

                success:function(res){

                    $("#supplire_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
        }

     



    $(function () {
    // Summernote
        $('.textarea').summernote();
    })

    </script>   

    <!-- PAGINATION RENDER -->
    <script>
        function renderPagination(totalPage) {
            $('#pagination-list').html("");
            let prev = '<li class="page-item" id="btn-prev"><a class="page-link" href="#">&laquo;</a></li>';
            $('#pagination-list').append(prev);
            for (let i = 0; i < totalPage; i++) {
                if ((i+1) == currentPage) {
                    let html = '<li class="page-item active pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }  else {
                    let html = '<li class="page-item pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }

            }
            let next = '<li class="page-item"><a class="page-link" href="#" id="btn-next">&raquo;</a></li>';
            $('#pagination-list').append(next);
        }

        $('body').on('click', '.pagination-item', function () {
            let current_page = $(this).find('a').html();
            currentPage = parseInt(current_page);
            fetch_data();
        })

        $('body').on('click', '#btn-prev', function () {
            currentPage --;
            fetch_data();
        })

        $('body').on('click', '#btn-next', function () {
            currentPage ++;
            fetch_data();
        })

    </script>



       



@endsection