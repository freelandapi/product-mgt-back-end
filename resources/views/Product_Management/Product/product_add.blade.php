@extends('Master.master')
@section('current_page_name', 'Product');
@section('content')

     <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Product Add</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              
              
            </div>
          </div>
          <!-- /.card-header -->

        <!-- Lelf Side -->
        <div class="card-body">
            <div class="row">
            	<div class="col-md-6">

	              	<div class="form-group">
	                  <label>Product Name</label>
	                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter product name">
                    <span class="text text-danger">{{$errors->first('name')}}</span>
	                </div>

	                <div class="form-group">
	                  <label>Category</label>
	                  <select class="form-control" style="width: 100%;" id="category">
	                    
	                  </select>
	                </div>
	               
	                <div class="form-group">
	                  <label>Brands</label>
	                  <select class="form-control"  style="width: 100%;" id="brand">
	                    
	                  </select>
	                </div>

	                <div class="form-group">
	                    <label for="exampleInputFile">File input</label>
	                    <div class="input-group">
	                      <div class="custom-file">
	                        <input type="file" class="custom-file-input" id="exampleInputFile">
	                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
	                      </div>
	                      <div class="input-group-append">
	                        <span class="input-group-text" id="">Upload</span>
	                      </div>
	                    </div>
                    </div>

            	</div>
             
            <!-- Right Side -->
              <div class="col-md-6">
                  <div class="form-group">
	                  <label>Product Code</label>
	                  <input type="text" name="code" class="form-control" placeholder="Enter product code" id="code">
	                </div>

                  <div class="form-group">
                    <label>Supplire</label>
                    <select class="form-control"  style="width: 100%;" id="supplire">
                      
                    </select>
                  </div> 

                <!-- option -->
                <div class="form-group float-left">
                  <label>Status</label>
            		<input type="checkbox" name="my-checkbox"  data-bootstrap-switch>
                </div>

              </div>
        	</div>
        	<!-- /.row -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            	<button type="submit" class="btn btn-primary float-right" id="btn_save" style="margin-left:5px !important;">Submit</button>
            	<button type="button" class="btn btn-danger float-right" id="btn_close">Close</button>
          </div>

        </div>
      </div>
    </section>
 

  <script>
    
      $(document).ready(function(){

          load_category();
          load_brand();
          load_supplire();

      });

      // Add Product 

       $('#btn_save').click(function (){

        let params = {
                name : $("#name").val(),
                code : $("#code").val(),
                category : parseInt($("#category").val()),
                brand:  parseInt($("#brand").val()),
                supplire : parseInt($("#supplire").val()),
        }

        if(params.name == null || params.name   == ""){

          shows_error("Please Input Name !!");

        } else if (params.code == null || params.code   == ""){

          shows_error("Please Input Code !!");

        } else {

          $.ajax({
                headers: HEADER,
                url: BASE_URL+"products",
                method: "POST",
                data: params,

                success:function(res){             
                    window.location.href = "{{url('products')}}";

                },
                error: function(e){

                }
            })
        }
       })

       $("#btn_close").click(function(){

          window.location.href = "{{url('products')}}";
       })

       function shows_error(errorMessage){

          swal("Error",errorMessage,"error");
       }


       // function category
       function load_category(){

          $.ajax({
                headers: HEADER,
                url: BASE_URL+"product_cate",
                method: "GET",
                
                success:function(res){

                  //console.log("product_cate:",res.data);
                  $.each(res.data, function(i, item){
                    $('#category').append(' <option value="'+item.id+'">'+item.name+'</option>')
                  });

                },
                error: function(e){


                }
            })

       }

       // function Brand
       function load_brand(){

          $.ajax({
                headers: HEADER,
                url: BASE_URL+"brands",
                method: "GET",
               
                success:function(res){

                  $.each(res.data, function(i, item){
                    $('#brand').append('<option value="'+item.id+'">'+item.name+'</option>')
                  });
                  
                },
                error: function(e){


                }
            })

       }

       // function supplire
       function load_supplire(){

          $.ajax({
                headers: HEADER,
                url: BASE_URL+"supplire",
                method: "GET",
               
                success:function(res){

                  $.each(res.data, function(i, item){
                    $('#supplire').append('<option value="'+item.id+'">'+item.FullName+'</option>')
                  });

                },
                error: function(e){


                }
            })

       }









  </script>



@endsection