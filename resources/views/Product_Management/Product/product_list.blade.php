
@extends('Master.master')
@section('current_page_name', 'Product List');
@section('content')


 <!--================ Font Awesome================= -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<style type="text/css">

    th,td{
      text-align: center;
    }
	/* show detail*/
		.product_view .modal-dialog{max-width: 1000px; width: 100%;}
        .pre-cost{text-decoration: line-through; color: #a5a5a5;}
        .space-ten{padding: 10px 0;}

        .checked {
		  color: orange;
		}

		div.red{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid red;
			  background-color: red;
			  border-radius: 100px;
		}

		div.blue{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid blue;
			  background-color: blue;
			  border-radius: 100px;
		}

		div.yellow{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid yellow;
			  background-color: yellow;
			  border-radius: 100px;
		}

</style>


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Product List</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">


                            <div class="row py-2" id="hide1">
                                    <div class="col-4">
                                    <a href="{{url('product_add')}}" class="btn btn-sm btn-primary btn-content add_new" style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                                    </div>
                                    <div class="col-8 my-auto mr-auto">
                                        <form class="form-inline mr-auto float-right">
                                            <input class="form-control mr-sm-2 enter_search" type="text" id="search" placeholder="Search Supplire" aria-label="Search Supplire">
                                            <button class="btn btn-cyan btn-rounded my-0"
                                                    id="txtUserSearh"
                                                    type="submit">
                                                <i class="fas fa-search btn_search"></i>
                                            </button>
                                        </form>
                                    </div>
                            </div>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th>#</th>
                                      <!--  <th>Profile</th> -->
                                      <th>Name</th>
                                      <th>Code</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="product_content">

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right" id="pagination-list">
                                <!-- page list here -->
                            </ul>
                        </div>
                        </div>


                    </div>

                    </div>

                </div>
            </section>


  <!-- ====== Modal Form  Show Detail ======  -->

    <div class="modal fade hidden product_view" id="ModalShow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

     	<div class="modal-dialog">
        	<div class="modal-content">
	            <div class="modal-header">
	               <!--  <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a> -->
	                <h3 class="modal-title">Show Detail</h3>
	                <button type="button" class="btn btn-tool" data-dismiss="modal" data-card-widget="remove"><i class="fas fa-times"></i></button>

	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-5">
	                        <img src="http://img.bbystatic.com/BestBuy_US/images/products/5613/5613060_sd.jpg" class="img-responsive" style="width:200px;">
	                    </div>
	                    <div class="col-md-7">
	                        <h4> Code: <span id="p_code"></span></h4>
	                        <div class="rating">
	                            <span class="glyphicon glyphicon-star"></span>
	                            <span class="glyphicon glyphicon-star"></span>
	                            <span class="glyphicon glyphicon-star"></span>
	                            <span class="glyphicon glyphicon-star"></span>
	                            <span class="glyphicon glyphicon-star"></span>
	                            (10 reviews)
	                        </div>
	                        <h3 class="text-success" id="p_name"></h3>
	                        <p id="p_desc"></p>
	                        <h3 class="cost"><span class="glyphicon glyphicon-usd"></span> 75.00 <small class="pre-cost"><span class="glyphicon glyphicon-usd"></span id="p_price"> 60.00</small></h3>

                          <b>Category : </b><span class="text-success" id="p_category"></span><br>
                          <b>Brand : </b><span class="text-success" id="p_brand">  </span><br>
                          <b>SUPPLIRE : </b><span class="text-success" id="p_supplire"></span><br>


	                        <!-- color option -->
                        	<h4 id="p_color">Color</h4>
                        	<div class="red"></div>
                        	<div class="blue"></div>
                        	<div class="yellow"></div>



	                    </div>
	                </div>
	            </div>
        	</div>
    	</div>

    </div>
    <!-- ==== End Modal Show ==== -->


    <script type="text/javascript">

        var totalPage = 0;
        var currentPage = 1;
        var perPage = 2;

        $(document).ready(function () {

            fetch_data();
            load_category(); 
           
            var url_string = window.location.href ;
            var url = new URL(url_string);
        });

        function fetch_data() {
            $("#product_content").html("");
            let params = {
                page: currentPage,
                size: perPage
            }
            $.ajax({

                headers: HEADER,
                url: BASE_URL + "products",
                method: "GET",
                data : params,

                success:function(res){

                  console.log("products1: ",res.data)
                    var products = "";
                    $.each(res.data,function(index,val){

                        products = '<tr>'
                                        +'<td class="id">'+ val.id+'</td>'
                                        +'<td>'+ val.name+'</td>'
                                        +'<td>'+ val.code+'</td>'

                                        +'<td>'

                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-info btn-sm btn_show" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-eye"></i></button>'

                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                                        + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                                        +'</td>'
                                    + '</tr>';

                        $("#product_content").append(products);
                    });
                        //PAGINATION
                        currentPage = res.current_page;
                        totalPage = res.total_record/perPage;
                        renderPagination(totalPage);
                },
                error: function(e){
                    console.log(e)
                }
            }); 
        }

        // Edit
    $("body").on("click",".btn_edit",function(){


        let id = $(this).attr("id");
        let data_item = $(this).attr("data-item");


        let data_edit = JSON.parse(decodeURIComponent(data_item));


        let name = data_edit.name;
        let code = data_edit.code;
        let category = data_edit.category_id;
        let brand = data_edit.brand_id ;
        let supplire = data_edit.supplire_id;

        let params = '?id='+id+'&name=' + name + '&code=' + code + '&category_id=' + category + '&brand_id=' + brand + '&supplire_id=' + supplire;

        //var encodedParam = encodeURIComponent(params);


        window.location.href = "{{url('product_edit')}}" + params ;

    });

    	// Show Detail
    $("body").on("click",".btn_show",function(){

      let id = $(this).attr("id");
      let data_item = $(this).attr("data-item");
      let current_item = JSON.parse(decodeURIComponent(data_item));
      
      console.log("data:",current_item)
      $("#p_code").text(current_item.code);
      $("#p_name").text(current_item.name);

      if(current_item.desc !== null){

        $("#p_desc").text(current_item.desc.desc);
      }else{
          $("#p_desc").text("");
      }
      


      //all set here bro, no need param, simple
      $('#p_category').html(current_item.category.name);
      $('#p_brand').html(current_item.brand.name);
      $('#p_supplire').html(current_item.supplire.FullName);

      $('#ModalShow').modal('show');
    });



     //========= Event Delete with Sweetalert ===========//

        $("body").on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();

          //  console.log("edit id:",id)
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_product(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
        }); 

        function delete_product(id){

            $.ajax({

                headers: HEADER,
                url: BASE_URL + 'products/' + id,
                method: "DELETE",

                success:function(res){

                    $("#product_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
        }
    </script>

    <!-- PAGINATION RENDER -->
    <script>
        function renderPagination(totalPage) {
            $('#pagination-list').html("");
            let prev = '<li class="page-item" id="btn-prev"><a class="page-link" href="#">&laquo;</a></li>';
            $('#pagination-list').append(prev);
            for (let i = 0; i < totalPage; i++) {
                if ((i+1) == currentPage) {
                    let html = '<li class="page-item active pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }  else {
                    let html = '<li class="page-item pagination-item"><a class="page-link" href="#">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }

            }
            let next = '<li class="page-item"><a class="page-link" href="#" id="btn-next">&raquo;</a></li>';
            $('#pagination-list').append(next);
        }

        $('body').on('click', '.pagination-item', function () {
            let current_page = $(this).find('a').html();
            currentPage = parseInt(current_page);
            fetch_data();
        })

        $('body').on('click', '#btn-prev', function () {
            currentPage --;
            fetch_data();
        })

        $('body').on('click', '#btn-next', function () {
            currentPage ++;
            fetch_data();
        })

    </script>

@endsection
