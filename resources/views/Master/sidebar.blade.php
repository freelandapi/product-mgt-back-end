
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{asset('assets/img/test_logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">MenZo Shop</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/img/default-avatar.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"><span id="loggedin_user"></span></a>
                <script>
                    
                    $('#loggedin_user').html(SAVED_USER.firstname+' '+SAVED_USER.lastname);
                </script>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" id="nav-sidebar">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <!-- Dashboard  -->
                <li class="nav-item">
                    <a href="{{url('dashboard')}}" class="nav-link @yield('active-dashboard-parent')">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <!-- Users Management-->
                <li class="nav-item has-treeview @yield('open-collapse-menu-userList')">
                    <a href="#" class="nav-link @yield('active-menu-parentUserList')">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('role')}}" class="nav-link @yield('active-menu-role')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('permissions')}}" class="nav-link @yield('active-menu-permission')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Permission</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('user')}}" class="nav-link @yield('active-menu-users')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>

                    </ul>
                </li>


                <!-- Product Management-->

                <!-- Media File Management-->
                <li class="nav-item has-treeview @yield('open-collapse-menu-fileCategory')">
                    <a href="#" class="nav-link @yield('active-menu-fileCategoryParents')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Media Files
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('media_files')}}" class="nav-link @yield('active-menu-file')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>File List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('media_files/category')}}" class="nav-link @yield('active-menu-fileCategory')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>File Category</p>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Product Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('products')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('pro_cate')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Category</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Supplire Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('supplire')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplire List</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('supplire_add')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplire Add</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Brand Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('brands')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Brand List</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- Menu Management -->
                <li class="nav-item has-treeview @yield('open-collapse-menu-menuList')">
                    <a href="#" class="nav-link @yield('active-menu-menuListParent')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Menu Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('menus/list')}}" class="nav-link @yield('active-menu-menuList')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Menu List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('submenus/list')}}" class="nav-link @yield('active-menu-submenuList')">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Submenu List</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- Setting -->
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Setting 
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('setting_list')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplire List</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('setting_add')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplire Add</p>
                            </a>
                        </li>
                    </ul>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

