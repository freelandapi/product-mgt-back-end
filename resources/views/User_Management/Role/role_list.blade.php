@extends('Master.master')
@section('current_page_name', 'Role')
@section('page_title','Role')
@section('content')


    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <style>
            th,td{
                text-align:center;
            }
   
    </style>

    
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Role List</h3>
                </div>
                <!-- /.card-header --> 
                <div class="card-body">

                    
                    <div class="row py-2" id="hide1">
                            <div class="col-4">
                            <a href="#" class="btn btn-sm btn-primary btn-content AddNew"  data-toggle="modal" data-target="#exampleModal" id="AddNew" style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                            </div>
                            <div class="col-8 my-auto mr-auto">
                                <form class="form-inline mr-auto float-right">
                                    <input class="form-control mr-sm-2 enter_search" type="text" id="search_text" placeholder="Search Name" aria-label="Search Name">
                                    <button type="button" class="btn btn-cyan btn-rounded my-0"
                                            id="btn_search">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </form>
                            </div>
                    </div>

                    <table class="table table-bordered">
                        <thead>                  
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="role_content">
                                        
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                    <!-- <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-right">
                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                        <li class="page-item active"><a class="page-link" href="#"> 1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                        </ul>
                    </div> -->
                </div>
            
    
            </div>
            
            </div>
    
        </div>
    </section>



    <!-- ====== Modal Form ====== -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Create Role</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="md-form mb-3">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control" id="name"  name="name" value="" placeholder="Role Name">
                        <div class="firstname-valid-feedback">
                        </div>
                        <span class="text-danger">{{$errors->first('name')}}</span>
                    </div>
                    <div class="md-form mb-3">
                        <i class="fas fa-key prefix grey-text"></i>
                        <input type="text" class="form-control" id="slug" name="slug" value="" placeholder="Slug Name">
                        <div class="firstname-valid-feedback">
                        </div>
                    </div>

                    <div class="md-form mb-3">

                        <i class="fas fa-file prefix grey-text"></i>
                        <textarea name="desc" id="desc" class="editor" placeholder="Enter Description"></textarea>
                        <div class="firstname-valid-feedback">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;">Close</button>
                    <button type="button" onclick="create_role(this)" class="btn btn-primary btn-sm  btn-content btn-content-add"  style="border-radius: 3.2rem;" data-dismiss="modal">Save</button>
                </div>


            </div>
        </div>
    </div>


    <!-- ====== Modal Form  Edit ====== -->

    <div class="modal fade hidden" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Edit Role</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="md-form mb-3">
                        <i class="fas fa-user prefix grey-text"></i>
                        <input type="text" class="form-control" id="e_name"  name="e_name" value="">
                        <div class="firstname-valid-feedback">
                        </div>
                    </div>
                    <div class="md-form mb-3">
                        <i class="fas fa-key prefix grey-text"></i>
                        <input type="text" class="form-control" id="e_slug" name="e_slug" value="">
                        <div class="firstname-valid-feedback">
                        </div>
                    </div>

                    <div class="md-form mb-3">
                        <i class="fas fa-file prefix grey-text"></i>
                        <textarea name="e_desc" id="e_desc" class="editor"></textarea>
                        <div class="firstname-valid-feedback">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;">Close</button>
                    <button type="button"  class="btn btn-primary btn-sm" id="btn_submit" style="border-radius: 3.2rem;" data-dismiss="modal">Update</button>
                </div>


            </div>
        </div>
    </div>



    <script type="text/javascript">

        $(document).ready(function () {
           
            fetch_data();
            // disable action form on click enter
            $('form').on('submit', function (e) {
                e.preventDefault();
            });

        });

        function fetch_data() {
            $.ajax({

                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: "http://127.0.0.1:8000/api/role",
                method: "GET",

                success:function(res){
                    appendData(res.data); 
                },
                error: function(e){
                    console.log(e)
                }
            });
        }

        $("document").on("click","#AddNew",function(){

        });

        //Append Data List
        function appendData(data) {
            $("#role_content").html("");
            var contents = "";
                    $.each(data,function(index,val){
                        contents = '<tr>'
                            +'<td class="id">'+ val.id+'</td>'
                            +'<td>'+ val.name+'</td>'
                            +'<td>'+ val.slug+'</td>'
                            +'<td>'+ val.description+'</td>'
                            +'<td>'
                            + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                            + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                            +'</td>'
                            + '</tr>';

                        $("#role_content").append(contents);
            });
        }
       
        // Add Product Category
        function create_role(click) {

            let Name = $("#name").val();
            let Slug = $("#slug").val();
            let Description = $("#desc").val();

            if(Name == '' || Name == null){
                shows_error("Please Input RoleName !!");
            }

            params = {
                name : Name,
                slug : Slug,
                description : Description
            }

            console.log("role:",params);

            
            $.ajax({
                headers: HEADER,
                url: BASE_URL+"role",
                method: "POST",
                data: params,

                success:function(res){
                    //console.log("Data :",res.data);
                    $("#name").val('');
                    $("#slug").val('');
                    $("#desc").val('');
                    $("#role_content").html('');
                    fetch_data();

                },
                error: function(e){
                    console.log("this is error:",e)
                }
                })
            
            
        }

        function shows_error(errorMessage){

            swal("Error",errorMessage,"error");
        }


        // show modal edit
        $("body").on("click",".btn_edit",function(){

            let item = $(this).attr("data-item");
            const obj = JSON.parse(decodeURIComponent(item));

            $("#btn_submit").attr("data-id", obj.id);
            $("#e_name").val(obj.name);
            $("#e_slug").val(obj.slug);
            $("#e_desc").val(obj.description);

            $('#ModalEdit').modal('show');
        });


        // click Update Button
        $("body").on("click",'#btn_submit',function () {

            let id = $(this).attr("data-id");
            edit_role(id);
        })

        // Edit Action
        function edit_role(id){
            var param = {
                name : $("#e_name").val(),
                slug : $("#e_slug").val(),
                description : $("#e_desc").val()
            };
            console.log("name:",param)
            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'role/' + id,
                method: "PUT",
                data: param,

                success:function(res){
                    $('#ModalEdit').modal('hide');
                    $("#role_content").html('');
                    fetch_data();
                },
                error: function(e){
                    swal("bad job", "You clicked the button!", "error");
                    console.log(e.responseText)
                }
            })
        }

        //========= Event Delete with Sweetalert ===========//

        $(document).on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_role(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
        });

        // function search 
        $(document).on("click", "#btn_search",function(){

            search();
        });

         // is hit enter key
       $(document).on("keydown","#search_text", ({key}) => {
            if (key === "Enter"){
                search();
            }
        })
        // search value is empty render data again
        $("#search_text").keyup(function(){
            if($(this).val() == ""){
                fetch_data();
            }
        })



        function search(){

            var search_val = $("#search_text").val();
            console.log(search_val);
            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'search/' + search_val,

                method: "GET",

                success:function(res){

                    appendData(res.data);
                },
                error: function(e){
                    console.log(e)
                }
            })
        }
        
        



        function delete_role(id){

            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'role/' + id,

                method: "DELETE",

                success:function(res){

                    $('#ModalEdit').modal('hide');
                    $("#role_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
        }


    </script>

@endsection
