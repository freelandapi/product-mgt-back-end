
@extends('Master.master')
@section('current_page_name', 'User')
@section('page_title', 'Users')

@section('active-menu-parentUserList', 'active')
@section('active-menu-users', 'active')
@section('open-collapse-menu-userList', 'menu-open')
@section('content')



    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <style> 
            th,td{
                text-align:center;
            }
    </style>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User List</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body"> 


                    <div class="row py-2" id="hide1">
                            <div class="col-4">
                            <a href="{{url('user_add')}}" class="btn btn-sm btn-primary btn-content add_new" style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                            </div>
                            <div class="col-8 my-auto mr-auto">
                                <form class="form-inline mr-auto float-right">
                                    <input class="form-control mr-sm-2 enter_search" type="text" id="search" placeholder="Search Supplire" aria-label="Search Supplire">
                                    <button class="btn btn-cyan btn-sm btn-rounded my-0"
                                            id="txtUserSearh"
                                            type="submit">
                                        <i class="fas fa-search btn_search"></i>
                                    </button>
                                </form>
                            </div>
                    </div>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Profile</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="user_content">

                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm m-0 float-right" id="pagination-list">
                        <!-- page list here -->
                    </ul>
                </div>
                </div>


            </div>

            </div>

        </div>   
    </section>

   <!-- Modal Show -->
   @include('User_Management.User.modal_show_user')



    <script type="text/javascript">
        var totalPage = 0;
        var currentPage = 1;
        var perPage = 10;

        $(document).ready(function () {

            fetch_data();
        });

        function fetch_data() {
            $("#user_content").html("");
            let params = {
                page: currentPage,
                size: perPage
            }
            $.ajax({
                headers: HEADER,
                url: BASE_URL+"users",
                method: "GET",
                data: params,
                success:function(res){
                    var contents = "";

                    let full_url = RAW_BASE_URL + 'default_user.png';
                    $.each(res.data,function(index,val){

                        contents = '<tr>'
                            +'<td class="id">'+ val.id+'</td>'
                            +'<td id="profile" class="profile">'
                             +   '<img src="'+(val.profile?val.profile:full_url) +'" style="width:50px;height:50px;">'
                            +'</td>'
                            +'<td>'+ val.firstname+ '</td>'
                            +'<td>'+ val.lastname+ '</td>'
                            +'<td>'+ val.gender+ '</td>'
                            +'<td>'+ val.phone+ '</td>'
                            +'<td>'

                              + ' <button type="button"  style="border-radius: 20px;" class="btn btn-secondary btn-sm btn_show" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-eye"></i></button>'

                            + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                            + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                            +'</td>'
                            + '</tr>';



                        // let profile = '<img src="'+full_url+'">';
                        //
                        // $(".profile").html(profile);

                        $("#user_content").append(contents);
                    });

                    //PAGINATION
                    currentPage = res.current_page;
                    totalPage = res.total_record/perPage;
                    renderPagination(totalPage);
                },
                error: function(e){
                    console.log(e)
                }
            });
        }
        // append profile

        let full_url = 'http://127.0.0.1:8000/storage/' + 'default_user.png';

        // Show Detail User
        $("body").on("click",".btn_show",function(){

            let item = $(this).attr("data-item");
            const obj = JSON.parse(decodeURIComponent(item));

            $("#s_email").text(obj.email);
            $("#s_fname").text(obj.firstname);
            $("#s_lname").text(obj.lastname);
            $("#s_address").text(obj.address);
            $("#s_desc").text(obj.description);
            $("#s_phone").text(obj.phone);
            $("#s_gender").text(obj.gender);

            $('#ModalShow').modal('show');
        });

        // Function Error
        function shows_error(errorMessage){
            swal("Error",errorMessage,"error");
        }

        // show modal edit
        $("body").on("click",".btn_edit",function(){

            let id = $(this).attr("id");
            let item = $(this).attr("data-item");
            const obj = JSON.parse(decodeURIComponent(item));

            let email = obj.email;
            let firstname = obj.firstname;
            let lastname = obj.lastname;
            let phone = obj.phone;
            let address = obj.address;
            let description = obj.description;
            let profile = obj.profile;
            let gender = obj.gender;

            let params = '?id='+id+'&firstname=' + firstname + '&lastname='+lastname +'&gender=' +gender+ '&email=' + email + '&phone=' + phone + '&address=' + address + '&description='+description+'&profile='+profile;
            
            window.location.href = "{{url('user_edit')}}" + params ;
           // $('#ModalEdit').modal('show');
        });

        //========= Event Delete with Sweetalert ===========//

        $(document).on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_user(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
        });
        // Delete User
        function delete_user(id){

            $.ajax({
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'users/' + id,

                method: "DELETE",

                success:function(res){
                    //$("#user_content").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
        }
        // Clear Form Data
        function clear(){
            for (let i = 1; i<=4; i++) {
                $("#value"+i).val('');
            }
            $("#address").val('');
            $("#phone").val('');
            $("#profile").val('');
            $("#desc").val('');
          
        }
    </script>

    <!-- PAGINATION RENDER -->
    <script>
        function renderPagination(totalPage) {
            $('#pagination-list').html("");
            let prev = '<li class="page-item" id="btn-prev"><a class="page-link" href="javascript:">&laquo;</a></li>';
            $('#pagination-list').append(prev);
            for (let i = 0; i < totalPage; i++) {
                if ((i+1) == currentPage) {
                    let html = '<li class="page-item active pagination-item"><a class="page-link" href="javascript:">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }  else {
                    let html = '<li class="page-item pagination-item"><a class="page-link" href="javascript:">'+(i+1)+'</a></li>';
                    $('#pagination-list').append(html);
                }

            }
            let next = '<li class="page-item"><a class="page-link" href="javascript:" id="btn-next">&raquo;</a></li>';
            $('#pagination-list').append(next);
        }

        $('body').on('click', '.pagination-item', function () {
            let current_page = $(this).find('a').html();
            currentPage = parseInt(current_page);
            fetch_data();
        })

        $('body').on('click', '#btn-prev', function () {
            if(currentPage <= 1){
                currentPage
            }else{
                currentPage --;
                fetch_data();
            }
            
        })

        $('body').on('click', '#btn-next', function () {
           
            if((currentPage >= 1) && (currentPage < parseInt(totalPage))){
                currentPage++;
                fetch_data();
            }else{
                currentPage
            }
        })

    </script>

@endsection
