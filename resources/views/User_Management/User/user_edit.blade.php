
@extends('Master.master')
@section('content')

<!-- sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

 <!-- ====== Modal Form ====== -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Update</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              
              
            </div>
          </div>
          <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <!-- Left-side -->
                        <div class="col-md-6">

                                <div class="form-group">
                                    <label for="exampleInputPassword1">First Name</label>
                                    <span><i class="fa fa-star" style="color:red;"></i></span>
                                    <input type="text" class="form-control first_name" id="e_firstname" placeholder="Firstname">
                                    <div class="FirstNameError" style="display:none"><p>First Name is required</p></div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Address</label>
                                    <span><i class="fa fa-star" style="color:red;"></i></span>
                                    <input type="text" class="form-control address" id="e_address" placeholder="Address">
                                    <div class="AddressError" style="display:none"><p>Address is required</p></div>
                                </div>


                                <!-- Gender -->
                                <label>Gender</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="male" id="male">Male</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="female" id="female">Female</label>

                                
                                <!-- File upload -->
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="e_profile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>

                                <!--  preview image-->
                                <div >
                                    <img style="width:150px;height:150px;object-fit: cover" id="preview-img">
                                </div>



                                <div class="form-group">
                                    <label>Textarea</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter Desc" id="e_desc"></textarea>
                                </div>

                        </div>
                        
                        <!-- Right Side -->
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Last Name</label>
                                <span><i class="fa fa-star" style="color:red;"></i></span>
                                <input type="text" class="form-control lastname" id="e_lastname" placeholder="Lastname">
                                <div class="LastNameError" style="display:none"><p>Last Name is required</p></div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Phone</label>
                                <span><i class="fa fa-star" style="color:red;"></i></span>
                                <input type="text" class="form-control phone" id="e_phone" placeholder="Phone">
                                <div class="PhoneError" style="display:none"><p>Phone Number is required</p></div>
                            </div>

    

                            <!-- <div class="form-group">
                                <label for="exampleInputPassword1">Profile</label>
                                <input type="text" class="form-control" id="profile">
                            </div> -->
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /.card-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;" id="btn_close">Close</button>
                    <button type="button"  class="btn btn-primary btn-sm  btn-content btn-content-add"  id="btn_submit" style="border-radius: 3.2rem;">Update</button>
                </div>

            </div>
        </div>
    </section>
        <!-- /.content -->
      

    <script>

        var files = "";
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-img').attr('src', e.target.result);
                    $('#preview-img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
                
            }
        }

    
    $(document).ready(function() {

        var url_string = window.location.href ;
        var url = new URL(url_string);
        var first_name = url.searchParams.get("firstname");

        $("#e_firstname").val(url.searchParams.get("firstname"));
        $("#e_lastname").val(url.searchParams.get("lastname"));
        $("#e_phone").val(url.searchParams.get("phone"));
        $("#e_desc").val(url.searchParams.get("description"));
        $("#e_address").val(url.searchParams.get("address"));
        
        // Gender 
        if(url.searchParams.get("gender")=="male"){
            $('#male').attr('checked', true)
        } else {
            $('#female').attr('checked', true)
        }
        
        if(url.searchParams.get("profile") != 'null') {
            $("#preview-img").attr("src", url.searchParams.get("profile"))
            
        } else {
            $("#preview-img").hide();
        }

        
        $('body').on('change','#e_profile', function() {
            readURL(this);
        })   
    })  

    // Update Action
    $(document).on("click","#btn_submit",function(){
    
        var url_string = window.location.href ;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");
        let base64 = $('#preview-img').attr('src');
        var FirstName = $("#e_firstname").val();
        var LastName = $("#e_lastname").val();
        var Password = $("#password").val();
        var Phone = $("#e_phone").val();
        var Address = $("#e_address").val();
        var Description = $("#e_desc").val();
        var Gender = $("input:radio[name=gender]:checked").val();
       // var Updated_by = SAVED_USER.id;


        // validation form 
        if(FirstName !== ""){
            
            $(".FirstNameError").hide();
            $(".first_name").css("border","");
        }else{
            
            $(".FirstNameError").css({"display":"","color":"red"});
            $(".first_name").css("border","1px solid red");
        }

        if(LastName !== ""){
            
            $(".LastNameError").hide();
            $(".lastname").css("border","");

        }else{
            $(".LastNameError").css({"display":"","color":"red"});
            $(".lastname").css("border","1px solid red");
        }

        if(Address !== ""){
            $(".AddressError").hide();
            $(".address").css("border","");
            
        }else{
            $(".AddressError").css({"display":"","color":"red"});
            $(".address").css("border","1px solid red");
        }

        if(Phone !== ""){   
            $(".PhoneError").hide();
            $(".phone").css("border","");
        }else{
            $(".PhoneIsNotNum").hide();
            $(".PhoneError").css({"display":"","color":"red"});
            $(".phone").css("border","1px solid red");
            
        }

        var params = {
                    firstname : FirstName,
                    lastname : LastName,
                    address: Address,  
                    phone : Phone,
                    description: Description, 
                   // updated_by : Updated_by,
                    gender: Gender       
        };

         // add file to object 
         if ($("#e_profile").val()) {
            params['profile'] = base64.replace('data:image/jpeg;base64,', '');
            var filePath = $("#e_profile").val(); 
            var file_ext = filePath.substr(filePath.lastIndexOf('.')+1,filePath.length);
            params['file_extension'] = file_ext.toLowerCase();
        }

        console.log("updated:",params); 
    
        $.ajax({
               headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                url: BASE_URL+'users/' + id,
                method: "POST",
                data : params ,

                success:function(res){   
                    console.log("sucess")              
                    window.location.href = "{{url('user')}}"; 
                },
                error: function(e){
                    console.log("faile")
                    //swal('error',e.responseJSON.message)
                }
        }); 
    });

    // Back to User list
    $("#btn_close").click(function(){
        window.location.href = "{{url('user')}}";
    });

    </script>
@endsection