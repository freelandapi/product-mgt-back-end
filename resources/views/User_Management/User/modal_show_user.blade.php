
  <!-- ====== Modal Form  Show Detail ======  -->

  <div class="modal fade hidden supplire_view" id="ModalShow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

<div class="modal-dialog">
   <div class="modal-content">
       <div class="modal-header">
          <!--  <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a> -->
           <h3 class="modal-title">SUser Detail</h3>
           <button type="button" class="btn btn-tool" data-dismiss="modal" data-card-widget="remove"><i class="fas fa-times"></i></button>

       </div>
       <div class="modal-body">
           <div class="row">
               <div class="col-md-5">
                   <img src="http://127.0.0.1:8000/default_user.png" alt="" class="img-circle img-fluid">
               </div>
               <div class="col-md-7">

                <span><b>First Name: </b></span>  <span id="s_fname"></span><br>
                <span><b>Last Name: </b></span>  <span id="s_lname"></span><br>
                <span><b>Gender: </b></span>  <b id="s_gender"></b><br>
                <span><b>Address: </b></span>  <span id="s_address"></span><br>
                <span><b>Phone : </b></span>  <span id="s_phone"></span><br>
                <span><b>Email : </b></span>  <span id="s_email"></span>

               </div>
           </div>
       </div>
   </div>
</div>

</div>
<!-- ==== End Modal Show ==== -->