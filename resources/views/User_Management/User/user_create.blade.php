@extends('Master.master')

@section('content')

<!-- sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

 <!-- ====== Modal Form ====== -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">User Add</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              
              
            </div>
          </div>
          <!-- /.card-header -->

              
                           
                <div class="card-body">
                    <div class="row">
                        <!-- Left-side -->
                        <div class="col-md-6">

                                <div class="form-group">
                                    <label for="exampleInputPassword1">First Name</label>
                                    <span><i class="fa fa-star" style="color:red;"></i></span>
                                    <input type="text" class="form-control first_name" id="f_name" placeholder="Firstname">
                                    <div class="FirstNameError" style="display:none"><p>First Name is required</p></div>
                                   
                                    
                                </div>

                                <!-- Multi select roles -->
                                <div class="form-group">
                                    <label>Multi Menu</label>
                                    <select class="select2bs4" multiple="multiple" data-placeholder="Select a State"
                                            style="width: 100%;" id="menu_by_user">
                                              
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Address</label>
                                    <span><i class="fa fa-star" style="color:red;"></i></span>
                                    <input type="text" class="form-control" id="address" placeholder="Address">
                                    <div class="AddressError" style="display:none"><p>Address is required</p></div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <span><i class="fa fa-star" style="color:red;"></i></span>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                                    <div class="EmailError" style="display:none"><p>Email Field is required</p></div>
                                    <div class="InvalideEmail" style="display:none"><p>Invalid Email Address</p></div>
                                    
                                </div>

                                <!-- Gender -->
                                <label>Gender</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="male" id="gender" checked>Male</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="female" id="gender">Female</label>

                                
                                <!-- File upload -->
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="profile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>

                                <!--  preview image-->
                                <div >
                                    <img style="width:150px;height:150px;object-fit: cover" id="preview-img">
                                </div>
                    
                                <div class="form-group">
                                    <label>Textarea</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter Desc" id="desc"></textarea>
                                </div>

                        </div>
                        
                        <!-- Right Side -->
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Last Name</label>
                                <span><i class="fa fa-star" style="color:red;"></i></span>
                                <input type="text" class="form-control" id="l_name" placeholder="Lastname">
                                <div class="LastNameError" style="display:none"><p>Last Name is required</p></div>
                                
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Phone</label>
                                <span><i class="fa fa-star" style="color:red;"></i></span>
                                <input type="text" class="form-control" id="phone" placeholder="Phone">
                                <div class="PhoneError" style="display:none"><p>Phone Number is required</p></div>
                                <div class="PhoneIsNotNum" style="display:none"><p>Phone Number must be Number Only</p></div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <span><i class="fa fa-star" style="color:red;"></i></span>
                                <input type="password" class="form-control" id="password" placeholder="Password">
                                <div class="PasswordError" style="display:none"><p>Password is required</p></div>
                             
                               
                            </div>

                            <!-- <div class="form-group">
                                <label for="exampleInputPassword1">Profile</label>
                                <input type="text" class="form-control" id="profile">
                            </div> -->
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /.card-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" style="border-radius: 3.2rem;" id="btn_close">Close</button>
                    <button type="button"  class="btn btn-primary btn-sm  btn-content btn-content-add"  id="btn_add" style="border-radius: 3.2rem;">Save</button>
                </div>

            </div>
        </div>
    </section>
        <!-- /.content -->
      

    <script>

    $("document").ready(function(){

        get_menu_by_user();

    });

    $('#preview-img').hide();

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-img').attr('src', e.target.result);
                    $('#preview-img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string 
        }
    }

    $("#profile").change(function() {
        readURL(this);
    });
       
        // Create User
        $('body').on('click','#btn_add',function(){
            
            create_user();       

        }); 
 
        function create_user() {
            
            var selected_items = $(".select2bs4").select2('data');
            var menu_ids = [];
           
            var FirstName = $("#f_name").val();
            var LastName = $("#l_name").val();
            var Email = $("#email").val();
            var Password = $("#password").val();
            var Phone = $("#phone").val();
            var Address = $("#address").val();
            var Description = $("#desc").val();
            var Gender = $("input[name='gender']:checked").val();
            var Created_by = SAVED_USER.id;
            var Menu_ids = menu_ids;

            // console.log("length:",Password.length);
            // console.log("password:",Password);
            
        
            // Loop Menus 
            $.each(selected_items,function(i,val){
                menu_ids.push(val.id)
            })

            // validation form 
            if(FirstName !== ""){
                
                $(".FirstNameError").hide();
                $(".first_name").css("border","");
            }else{
               
                $(".FirstNameError").css({"display":"","color":"red"});
                $(".first_name").css("border","1px solid red");
            }

            if(LastName !== ""){
                
                $(".LastNameError").hide();
                $("#l_name").css("border","");

            }else{
                $(".LastNameError").css({"display":"","color":"red"});
                $("#l_name").css("border","1px solid red");
            }

            if(Address !== ""){
                $(".AddressError").hide();
                $("#address").css("border","");
               
            }else{
                $(".AddressError").css({"display":"","color":"red"});
                $("#address").css("border","1px solid red");
            }

            if(Phone !== ""){   
                $(".PhoneError").hide();
                $("#phone").css("border","");
            }else{
                $(".PhoneIsNotNum").hide();
                $(".PhoneError").css({"display":"","color":"red"});
                $("#phone").css("border","1px solid red");
               
            }

        
            // let isnum = /^\d+$/.test(Phone);
            // if (!(isnum)) {
            //     $(".PhoneError").hide();
            //     $(".PhoneIsNotNum").css({"display":"","color":"red"});
            //     $("#phone").css("border","1px solid red");
            // }
            // Checking Empty Fields
		
            if ($.trim(Email).length == 0 )
            {
                $(".EmailError").css({"display":"","color":"red"});
                $(".InvalideEmail").css("display","none");
                $("#email").css("border","1px solid red");
                
            }else{
                $(".EmailError").hide();
                $("#email").css("border","");
            }

            // 2. check if invalid Email
            // if(isEmail(Email)){ 
            //     $(".InvalideEmail").css("display","none");
            //     $("#email").css("border","");
            //    // return true;
            // }else{
            //     $(".InvalideEmail").css({"display":"","color":"red"});
            //     $("#email").css("border","1px solid red");
            //     $(".EmailError").css("display","none");
            //     //return false;
            // }

            // // password
            if(Password !== "" || Password !== null || Password !== "null" || typeof(Password) !== "undefined"){
                
                $(".PasswordError").css({"display":"","color":"red"});
                $("#password").css("border","1px solid red");   
            }else{
                $(".PasswordError").hide();
                $("#password").css("border","");   
            }
        
        
            
            // Object
            var params = {
                firstname : FirstName,
                lastname : LastName,
                phone : Phone,
                address : Address,
                email : Email,
                password : Password,
                description : Description,
                gender : Gender,
                created_by : Created_by,
                menu_ids: Menu_ids
            }

            // add file to object
            let base64 = $('#preview-img').attr('src');
            if(typeof base64 != 'undefined') {
                params['profile'] = base64.replace('data:image/jpeg;base64,', '');
            }

             console.log("data",params);
          
        
            $.ajax({
                headers: HEADER,
                url: BASE_URL+"users",
                method: "POST",
                data: params,
                success:function(res){
                    window.location.href = "{{url('user')}}";
                },
                error: function(e){
                    //swal('error',e.responseJSON.message)
                    //console.log("this is error:",e.responseJSON.message)
                }
            })        
        }

        // Back to User list
        $("#btn_close").click(function(){
            window.location.href = "{{url('user')}}";
        })

        // function Get Role 
       function get_menu_by_user(){

            $.ajax({
                headers: HEADER,
                url: BASE_URL+"user_menu",
                method: "GET",
                data: {
                    "user_id" : SAVED_USER.id
                },

                success:function(res){

                    $.each(res.data, function(i, item){
                    $('#menu_by_user').append('<option value="'+item.id+'">'+item.name+'</option>')
                    });             
                },
                error: function(e){

                }
            })
        }

        function isEmail(email) { 
            return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
        } 

        

    </script>
@endsection