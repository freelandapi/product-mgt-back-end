@extends('Master.master')
@section('current_page_name', 'File Category')
@section('active-menu-parent', 'active')
@section('active-menu-file-category', 'active')
@section('open-collapse-menu', 'menu-open')
@section('page_title', 'Media file category')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <button type="button" class="btn btn-primary btn-sm" id="btn-create-file-cate"><i class="fa fa-fw fa-plus"></i></button>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th style="width: 40px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>


{{-- FORM MODAL  --}}
<div class="modal fade" id="show-form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New File Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="cate-title" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="cate-title">
                    </div>
                    <div class="form-group">
                        <label for="cate-desc" class="col-form-label">Description:</label>
                        <textarea class="form-control" id="cate_desc"></textarea>
                    </div>
                    <label for="cate-icon">iCon:</label>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Choose file
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" id="from-gallery">Choose from gallery</a>
                            <a class="dropdown-item" id="choose-from-pc">Choose from computer</a>
                        </div>
                    </div>
                    <div class="input-group" style="display: none;">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="cate-icon">
                            <label class="custom-file-label" for="cate-icon">Choose file</label>
                        </div>
                    </div>
                    <div class="mt-2">
                        <img id="preview-img" style="width: 100px; height: 100px; object-fit: cover;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-save">Save</button>
            </div>

        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="testmodal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gallery</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="gallery-list">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close-modal-gallery">Close</button>
            </div>
        </div>
    </div>
    <script>
        //On LOAD Gallery Modal

    </script>
</div>



<script>
    var iconFromGallery = true;

    $.ajax({
        url: BASE_URL + 'files_category'
        , method: 'GET'
        , headers: HEADER
        , success: function(res) {
            //console.log('res = ', res.data)
            $.each(res.data, function(index, item) {
                let tblRow = '<tr>' +
                    '             <td>' + (index + 1) + '.</td>' +
                    '             <td>' + item.title + '</td>' +
                    '             <td>' + item.desc + '</td>' +
                    '             <td>' +
                    '                <button class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>' +
                    '                <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#show-form-modal" id="btn-edit"><i class="fa fa-fw fa-edit"></i></button>' +
                    '             </td>' +
                    '         </tr>';

                $('tbody').append(tblRow);
            })
        }
        , error: function(error) {
            console.log('error = ', error)
        }
    });



    $.ajax({
        url: BASE_URL + 'files'
        , method: 'GET'
        , header: HEADER
        , success: function(res) {
            $.each(res.data, function(index, file) {
                let html = '<div class="col-md-3">' +
                    '<img src="' + file.file_url + '" class="img-fluid"/>' +
                    '</div>';
                $('#gallery-list').append(html);
            });
        }
        , error: function(error) {
            console.log('error fetch media file = ', error)
        }
    });


    //ON CLICK CREATE NEW FILE CATEGORY
    $('#btn-create-file-cate').click(function () {
        window.location.href = "{{url('media_files/category/create')}}"
    });




    //On CLICK BTN SAVE
    $('#btn-save').click(function() {
        let title = $('#cate-title').val();
        let desc = $('#cate_desc').val();
        let icon = $('#preview-img').attr('src');

        if (icon != null) {
            console.log('from gallery = ', icon.includes('data:image/'));
        }
 

        //console.log('title = ', title, 'desc = ', desc, 'icon = ', icon);
    })

    $('#preview-img').hide();

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#preview-img').attr('src', e.target.result);
                $('#preview-img').show();
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    //ON Change File
    $('#cate-icon').change(function() {
        readURL(this);
    });


    //on Click Choose from computer
    $('#choose-from-pc').click(function() {
        $('#cate-icon').click();
    })

</script>


<script>
    $(document).ready(function() {
        $('#gallery-list')

        $("#from-gallery").click(function() {
            $("#testmodal").modal("show");
        });

        $('#close-modal-gallery').click(function() {
            $("#testmodal").modal("hide");
        })
        //$("#testmodal").on('shown.bs.modal', function(e) {
        //console.log('The modal is about to be shown.');
        //    $('#show-form-modal').modal('show');
        //    $('#show-form-modal').css('display', 'block');
        //});


        function loadGallery() {
            $.ajax({
                url: BASE_URL + 'files'
                , method: 'GET'
                , header: HEADER
                , success: function(res) {
                    $.each(res.data, function(index, file) {
                        let html = '<div class="col-md-3">' +
                            '<img src="' + file.file_url + '" class="img-fluid"/>' +
                            '</div>';
                        $('#gallery-list').append(html);
                    });
                }
                , error: function(error) {
                    console.log('error fetch media file = ', error)
                }
            });
        }
    });

</script>

@endsection
