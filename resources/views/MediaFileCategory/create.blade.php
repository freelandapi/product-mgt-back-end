@extends('Master.master')
@section('current_page_name', 'Create Media File Category')
@section('active-menu-fileCategoryParents', 'active')
@section('active-menu-fileCategory', 'active')
@section('open-collapse-menu-fileCategory', 'menu-open')
@section('page_title', 'Create Media File Category')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create Media File Category</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>


                    </div>
                </div>
                <!-- /.card-header -->

                <!-- Lelf Side -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" id="title" class="form-control" placeholder="Enter category title">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Icon</label>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle form-control" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Choose file
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#" id="cf_media_file"><i class="fa fa-image"></i> Choose from media file</a>
                                        <a class="dropdown-item" href="#" id="cf_computer"><i class="fa fa-folder-open"></i> Choose from computer</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;">
                                <img class="img-fluid" width="100" height="100">
                            </div>

                        </div>

                        <!-- Right Side -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" placeholder="Enter file category description" id="desc" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right" id="btn_save" style="margin-left:5px !important;">Save</button>
                </div>

            </div>
        </div>
    </section>
    @include('.MediaFiles/file_list_modal')
    <script>
        $('body').on('click', '#cf_media_file', function (){
            $('#media_file_modal').modal('show')
        })
        $('body').on('click', '#cf_computer', function (){
            alert('click choose from pc')
        })
    </script>
@endsection
