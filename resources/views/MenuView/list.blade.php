
@extends('Master.master')
@section('current_page_name', 'Menu List')
@section('active-menu-menuListParent', 'active')
@section('active-menu-menuList', 'active')
@section('open-collapse-menu-menuList', 'menu-open')
@section('content')

    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title" style="height: 20px;">
                                <button class="btn btn-sm btn-primary" id="btn-create-menu"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Url</th>
                                    <th width="30%">Description</th>
                                    <th width="30%">Submenus</th>
                                    <th>Icon</th>
                                    <th width="100%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="menu_content">
                                    {{-- --}}


                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>

        <div id="modalContent">

        </div>
    </section>

    <script>
        $(document).ready(function () {
            $.ajax({
                headers: HEADER,
                data: {role_id: 1},
                method: 'GET',
                url: BASE_URL + 'menus',
                success: function (res) {
                    //console.log('success = ',res);

                    $.each(res.data, function (index, item) {
                        let tblContent = '<tr>' +
                            '<td>' + (index + 1) + '</td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.url + '</td>' +
                            '<td>' + item.desc + '</td>' +
                            '<td class="ml-auto" id="item' + index + '">' +

                            '</td>' +
                            '<td>' +
                            '<img src="' + item.icon + '" style="width: 50px; height: 50px;">' +
                            '</td>' +
                            '<td>' +
                            '<button type="button" class="btn btn-info btn-sm btn_show" id="' + item.id + '" data-item="' + encodeURIComponent(JSON.stringify(item)) + '"><i class="fa fa-fw fa-eye"></i></button>' +
                            '<button type="button" class="btn btn-success btn-sm btn-edit" id="' + item.id + '" data-item="' + encodeURIComponent(JSON.stringify(item)) + '"><i class="fa fa-fw fa-edit"></i></button>' +
                            '<button class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>' +
                            '</td>' +
                            '</tr>';
                        $('#menu_content').append(tblContent);
                    });

                    //MARK: - Append submenus
                    $.each(res.data, function (i, item) {
                        $.each(item.submenus, function (idx, sub) {
                            $('#item' + i).append('<p>&#45; ' + sub.name + '</p>');
                        });
                    });
                },
                error: function (error) {
                    console.log(' error = ', error)
                }
            });

            //LOAD MODAL
            $('#modalContent').load('./formModal');

            //ON CLICK CREATE
            $('#btn-create-menu').click(function () {
                //$('#modal-name').html('Create Menu');
                //$('#formModal').modal('show');
                window.location.href = "{{url('menu/create')}}";
            })

            //ON CLICK EDIT
            $('body').on('click', '.btn-edit', function () {
                $('#modal-name').html('Edit Menu');
                $('#formModal').modal('show');
            })
        });
    </script>
@endsection
