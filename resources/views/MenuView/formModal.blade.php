<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="modal-name">Create Menu</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="md-form mb-3">
                    <i class="fa fa-align-center prefix grey-text"></i>
                    <input type="text" class="form-control" id="name"  name="name" value="" placeholder="Enter name">
                </div>

                <div class="md-form mb-3">
                    <i class="fa fa-link prefix grey-text"></i>
                    <input type="text" class="form-control" id="url"  name="url" value="" placeholder="Enter url">
                </div>

                <div class="md-form mb-3">
                    <i class="fa fa-file prefix grey-text"></i>
                    <textarea type="text" class="form-control" id="desc"  name="desc" value="" placeholder="Enter Desc"> </textarea>
                </div>
                <div class="md-form mb-3">
                    <i class="fa fa-file prefix grey-text"></i>
                    <input type="file" class="form-control" id="file">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="btn-close" style="border-radius: 3.2rem;">Close</button>
                <button type="button" class="btn btn-primary btn-sm  btn-content btn-content-add" id="btn_create" style="border-radius: 3.2rem;" data-dismiss="modal">Save</button>
            </div>


        </div>
    </div>
</div>
