@extends('Master.master')
@section('current_page_name', 'Create Menu')
@section('active-menu-parent', 'active')
@section('active-menu-menuList', 'active')
@section('open-collapse-menu', 'menu-open')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create Menu</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>


                    </div>
                </div>
                <!-- /.card-header -->

                <!-- Lelf Side -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Product Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Enter product name">
                            </div>

                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" style="width: 100%;" id="category">

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Brands</label>
                                <select class="form-control"  style="width: 100%;" id="brand">

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Right Side -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Product Code</label>
                                <input type="text" name="code" class="form-control" placeholder="Enter product code" id="code">
                            </div>

                            <div class="form-group">
                                <label>Supplire</label>
                                <select class="form-control"  style="width: 100%;" id="supplire">

                                </select>
                            </div>

                            <!-- option -->
                            <div class="form-group float-left">
                                <label>Status</label>
                                <input type="checkbox" name="my-checkbox"  data-bootstrap-switch>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right" id="btn_save" style="margin-left:5px !important;">Submit</button>
                    <button type="button" class="btn btn-danger float-right" id="btn_close">Close</button>
                </div>

            </div>
        </div>
    </section>
@endsection
