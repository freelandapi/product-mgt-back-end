@extends('Master.master')
@section('current_page_name', 'Permission List')
@section('active-menu-parentUserList', 'active')
@section('active-menu-permission', 'active')
@section('open-collapse-menu-userList', 'menu-open')
@section('content')

    <!--================ Font Awesome================= -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <p>Permissions</p>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="role-list-content">
                                {{-- Rulle list --}}
                            </div>
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Url</th>
                                    <th width="30%">Description</th>
                                    <th width="30%">Submenus</th>
                                    <th>Read</th>
                                    <th>Write</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="menu_content">
                                    {{-- --}}


                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>

        <div id="modalContent">

        </div>
    </section>

    <style>
        div#role-list-content {
        background-color: #ccc;
        overflow: auto;
        height: 50px;
        white-space: nowrap;
        }

        div#role-list-content a {
            display: inline-block;
            color: white;
            text-align: center;
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 10px;
            height: 30px;
            padding-left: 15px;
            padding-top: 2px;
            padding-right: 15px;
            text-decoration: none;
            border-radius: 15px;
        }

        div#role-list-content a:hover {
        background-color: var(--mainColor);
        }
        .active-role {
            background-color: var(--mainColor);
        }


        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
            float:right;
        }

        /* Hide default HTML checkbox */
        .switch input {display:none;}

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: var(--customGray);
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        input.success:checked + .slider {
            background-color: var(--mainColor);
        }
        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }
        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }
        .slider.round:before {
            border-radius: 50%;
        }

    </style>


    <script>
        $(document).ready(function () {
            getRoleList();


            //ON CLICK ROLE ITEM
            $('body').on('click', '.role_name', function() {
                //$('#menu_content').html('');
                let role_id = $(this).attr('data-role-id');
                console.log('you clicked on role id = ', role_id);
            });


            //GET ROLE LIST
            function getRoleList() {
                $.ajax({
                    headers: HEADER,
                    method: 'GET',
                    url: BASE_URL + `role_by_user/${SAVED_USER.id}`,
                    success: function(res) {
                        $.each(res.data, function(index,role) {
                            if (index == 0) {
                                getMenuByUser();
                            }
                            const html = `<a href="javascript:" class="role_name ${index == 0 ? 'active-role' : ''}" data-role-id=${role.id}>${role.name}</a>`;
                            $('#role-list-content').append(html);
                        });

                        //ON CLICK ON Rolle
                        $('body').on('click', '.role_name', function(){
                            //let parent = $(this).parent();
                            $('.active-role').removeClass('active-role')
                            $(this).addClass('active-role');
                        })  
                    },
                    error: function(err) {
                        console.log('error = ', err)
                    }
                });
            }


            //GET MENU BY USER
            function getMenuByUser() {
                $.ajax({
                    headers: HEADER,
                    data: {user_id: SAVED_USER.id},
                    method: 'GET',
                    url: BASE_URL+'user_menu',
                    success: function(res) {
                        $.each(res.data, function (index, item) {
                            let tblContent = '<tr>' +
                                '<td>' + (index + 1) + '</td>' +
                                '<td>' + item.name + '</td>' +
                                '<td>' + item.url + '</td>' +
                                '<td>' + item.desc + '</td>' +
                                '<td class="ml-auto" id="item' + index + '">' +

                                '</td>' +
                                '<td>' +
                                    '<label class="switch">'+
                                        '<input type="checkbox" class="success" checked="checked">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</tr>';
                            $('#menu_content').append(tblContent);
                        });

                        //MARK: - Append submenus
                        $.each(res.data, function (i, item) {
                            $.each(item.submenus, function (idx, sub) {
                                $('#item' + i).append('<p>&#45; ' + sub.name + '</p>');
                            });
                        });
                    },
                    error: function(error){
                        console.log('error fetch menu = ', error);
                    }
                });
            }

            //GET MENU BY ROLE
            function getMenuByRole($role_id) {
                $.ajax({
                    headers: HEADER,
                    data: {role_id: $role_id},
                    method: 'GET',
                    url: BASE_URL + 'menus',
                    success: function (res) {
                        //console.log('success = ',res);

                        $.each(res.data, function (index, item) {
                            let tblContent = '<tr>' +
                                '<td>' + (index + 1) + '</td>' +
                                '<td>' + item.name + '</td>' +
                                '<td>' + item.url + '</td>' +
                                '<td>' + item.desc + '</td>' +
                                '<td class="ml-auto" id="item' + index + '">' +

                                '</td>' +
                                '<td>' +
                                    '<label class="switch">'+
                                        '<input type="checkbox" class="success" checked="checked">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</td>' +
                                '<td>' +
                                    '<label class="switch ">'+
                                        '<input type="checkbox" class="success">'+
                                        '<span class="slider round"></span>'+
                                    '</label>'+
                                '</td>' +
                                '</tr>';
                            $('#menu_content').append(tblContent);
                        });

                        //MARK: - Append submenus
                        $.each(res.data, function (i, item) {
                            $.each(item.submenus, function (idx, sub) {
                                $('#item' + i).append('<p>&#45; ' + sub.name + '</p>');
                            });
                        });
                    },
                    error: function (error) {
                            console.log(' error = ', error)
                    }
                });
            } 




            //LOAD MODAL
            $('#modalContent').load('./formModal');

            //ON CLICK CREATE
            $('#btn-create-menu').click(function () {
                //$('#modal-name').html('Create Menu');
                //$('#formModal').modal('show');
                window.location.href = "{{url('menu/create')}}";
            })

            //ON CLICK EDIT
            $('body').on('click', '.btn-edit', function () {
                $('#modal-name').html('Edit Menu');
                $('#formModal').modal('show');
            })
        });
    </script>
@endsection
