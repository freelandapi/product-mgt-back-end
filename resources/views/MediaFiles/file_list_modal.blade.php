<div class="modal fade" id="media_file_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Media File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-select form-control">
                                                <option selected>All</option>
                                                <option value="1">Product</option>
                                                <option value="2">User</option>
                                                <option value="3">iCon</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" placeholder="Search">
                                        </div>
                                    </div>
                                </div>

                                <div class="card card-primary">
                                    <div class="card-body">
                                        <div>
                                            <div class="p-0 row" id="file-content">

                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-footer clearfix">
                                        <ul class="pagination pagination-sm m-0 float-right" id="pagination-list">
                                            <li class="page-item" id="btn-prev"><a class="page-link" href="#">&laquo;</a></li>
                                            <li class="page-item" id="btn-prev"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item" id="btn-prev"><a class="page-link" href="#">2</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    const img = '<img src="https://i2.wp.com/ceklog.kindel.com/wp-content/uploads/2013/02/firefox_2018-07-10_07-50-11.png" class="img-fluid img-thumbnail" style="width:100px;height:100px; object-fit: cover; cursor: pointer;"/>';
    for (let i = 0; i < 10; i++) {
        $('#file-content').append(img);
    }

    $.ajax({

    });

</script>
