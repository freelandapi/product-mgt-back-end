@extends('Master.master')
@section('current_page_name', 'Add new file');
@section('content')


    <!-- Pop up message -->
    <section class="content">

            <div class="container-fluid" id="alert_popup" style="display:none">
                <div class="alert  alert-dismissible fade show" role="alert" style="background:rgba(0,100,0,0.5)">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                </div>
            </div>

    </section>


    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Add new file</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="email" class="form-control" id="name" placeholder="Enter Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Category</label>
                            <select class="form-control">
                                <option selected="selected" value="1">Alabama</option>
                                <option value="2">Alaska</option>
                                <option value="3">California</option>
                                <option value="4"> Delaware</option>
                                <option value="5">Tennessee</option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea class="form-control" rows="3" placeholder="Enter Description" id="desc" name="desc"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <!--  preview image-->
                    <div >
                        <img style="width:150px;height:150px;object-fit: cover" id="preview-img">
                    </div>

                    <div class="card-footer">
                        <button type="button" class="btn btn-primary" id="btn-save">Save</button>
                    </div>
                </form>


            </div>
        </div>
    </section>

    <script>
        $('#preview-img').hide();
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-img').attr('src', e.target.result);
                    $('#preview-img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#file").change(function() {
            readURL(this);
        });

        //on click btn save
        $('#btn-save').click(function (){

            let ext = $('#file').val().split('.').pop();
            let base64 = $('#preview-img').attr('src');
            const params = {
                name: $('#name').val(),
                desc: $('#desc').val(),
                category_id: 1,
                file: base64.replace('data:image/jpeg;base64,', ''),
                file_extension: ext
            }
           // console.log("data:",params)
            $.ajax({
                url: BASE_URL+'files',
                type: 'POST',
                data: params,
                cache: false,
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                success: function (res) {
                    //console.log(res);
                    $("#alert_popup").css("display","");
                    window.location.href = "{{url('media_files')}}";

                },
                error: function (error) {

                   alert(error.responseJSON.message)
                }
            });
        })





        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

    </script>
@endsection
