
@extends('Master.master')

@section('current_page_name', 'Gallery')
@section('active-menu-parent', 'active')
@section('active-menu-file', 'active')
@section('open-collapse-menu', 'menu-open')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">

                    <div class="card card-primary">
                        <div class="card-header">

                            <div class="ml-auto ">
                                {{-- btn add new --}}
                                <a href="{{url('media_files/new')}}" class="btn btn-sm  btn-content" id="btn-add_new" style="border-radius:20px;background-color:white;color:black;"><i class="fa fa-fw fa-plus"></i>Add New</a>

                            </div>
                        </div>
                        <div class="card-body">
                            <div>
                                <div class="btn-group w-100 mb-2">
                                    <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="1"> Category 1 (WHITE) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="2"> Category 2 (BLACK) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="3"> Category 3 (COLORED) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="4"> Category 4 (COLORED, BLACK) </a>
                                </div>
                                <div class="mb-2">
                                    <a class="btn btn-secondary" href="javascript:void(0)" data-shuffle> Shuffle items </a>
                                    <div class="float-right">
                                        <select class="custom-select" style="width: auto;" data-sortOrder>
                                            <option value="index"> Sort by Position </option>
                                            <option value="sortData"> Sort by Custom Data </option>
                                        </select>
                                        <div class="btn-group">
                                            <a class="btn btn-default" href="javascript:void(0)" data-sortAsc> Ascending </a>
                                            <a class="btn btn-default" href="javascript:void(0)" data-sortDesc> Descending </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="filter-container p-0 row">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <script>
        removeDefaultActiveMenu = true
        $(document).ready(function() {

            $.ajax({
                url: BASE_URL+'files',
                cache: false,
                type: "GET",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem('ACCESS_TOKEN')
                },
                success: function(res) {
                    $.each(res.data, function (index, item) {
                        let html = '<div class="filtr-item col-sm-2" data-category="1" data-sort="white sample">' +
                            '            <a>' +
                            '                <img src="'+item.file_url+'" class="img-fluid mb-2 file_content" alt="white sample" data-index="'+index+'" style="width:200px;height:200px; object-fit: cover; cursor: pointer;"/>' +
                            '            </a>' +
                            '      </div>'

                        $('.filter-container').append(html);
                    });

                    //CLICK BROWSE FILE
                    const urlParams = new URLSearchParams(window.location.search);
                    const isBrowseFile = urlParams.get('browse_file');
                    if (isBrowseFile){
                        $('.file_content').click(function(){
                            let index = $(this).attr('data-index');
                            let selectedFile = res.data[index].unique_name;
                            window.close();
                        });
                    }

                },
                error: function(err) {
                    console.log("fail error = ", err)

                }
            });

        });
    </script>
@endsection
