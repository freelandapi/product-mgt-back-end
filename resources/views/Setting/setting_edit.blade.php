@extends('Master.master')
@section('current_page_name', 'Setting');
@section('content')

     <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Setting Update</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              
              
            </div>
          </div>
          <!-- /.card-header -->

        <!-- Lelf Side -->
        <div class="card-body">
            <div class="row">
            	<div class="col-md-6">

	              	<div class="form-group">
	                  <label>Website Name</label>
	                  <input type="text" name="web_name" id="web_name" class="form-control" placeholder="Enter Website Name">
	                </div>

	                <div class="form-group">
	                  <label>Website Url</label>
                        <input type="text" name="web_url" id="web_url" class="form-control" placeholder="Enter Website URL">
	                </div>
	               
	                <div class="form-group">
	                  <label>Address</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="Enter Address">
	                </div>

                    <div class="form-group">
	                  <label>Phone</label>
	                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter Phone">
	                </div>

                  <div class="form-group">
	                  <label>Email</label>
	                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email">
	                </div>

	                <!-- <div class="form-group">
	                    <label for="exampleInputFile">File input</label>
	                    <div class="input-group">
	                      <div class="custom-file">
	                        <input type="file" class="custom-file-input" id="exampleInputFile">
	                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
	                      </div>
	                      <div class="input-group-append">
	                        <span class="input-group-text" id="">Upload</span>
	                      </div>
	                    </div>
                    </div> -->

            	</div>
             
            <!-- Right Side -->
              <div class="col-md-6">
                  <div class="form-group">
	                  <label>Logo Image</label>
	                  <input type="text" name="logo_image" id="logo_image" class="form-control" placeholder="Enter Logo" >
	                </div>

                  <div class="form-group">
                    <label>Google Map</label>
                    <input type="text" name="google_map" id="google_map" class="form-control" placeholder="Enter Google Map Address" >
                  </div> 

            
                <div class="form-group">
                  <label>Facebook Link</label>
                  <input type="text" name="fb_link" id="fb_link" class="form-control" placeholder="Enter Facebook Link">
                </div>

                <div class="form-group">
                  <label>Twitter Link</label>
                  <input type="text" name="twitter_link" id="twitter_link" class="form-control" placeholder="Enter Twitter Link">
                </div>
 
              </div>
        	</div>
        	<!-- /.row -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            	<button type="button" class="btn btn-primary float-right" id="edit_save" style="margin-left:5px !important;">Update</button>
            	<button type="button" class="btn btn-danger float-right" id="btn_close">Close</button>
          </div>

        </div>
      </div>
    </section>
 
  <script>
    
   

      $(document).ready(function(){

        var url_string = window.location.href ;
        var url = new URL(url_string);
   
          // Set Values to Form Edit
          $("#web_name").val(url.searchParams.get("website_name"));
          $("#web_url").val(url.searchParams.get("website_url"));
          $("#address").val(url.searchParams.get("address"));
          $("#phone").val(url.searchParams.get("phone"));
          $("#email").val(url.searchParams.get("email"));
          $("#logo_image").val(url.searchParams.get("logo_image"));
      
          $("#google_map").val(url.searchParams.get("google_map"));
          $("#fb_link").val(url.searchParams.get("fb_link"));
          $("#twitter_link").val(url.searchParams.get("twitter_link"));
         


         // show modal edit
        $("body").on("click","#edit_save",function(){
        

        var id = url.searchParams.get("id");
      
        let params = {
                website_name : $("#web_name").val(),
                website_url : $("#web_url").val(),
                address : $("#address").val(),
                phone:  $("#phone").val(),
                email : $("#email").val(),
                logo_image : $("#logo_image").val(),
                fb_link : $("#fb_link").val(),
                google_map : $("#google_map").val(),
                twitter_link : $("#twitter_link").val(),
                user_id: SAVED_USER.id
        }

        console.log("save user:",SAVED_USER); 

        $.ajax({
            headers: HEADER,
            url: BASE_URL + 'settings/' + id,
            method: "PUT",
            data : params ,

            success:function(res){
              console.log(" res = ",res.message);
              alert("hello")
             // window.location.href = "{{url('setting_list')}}";           
            },
            error: function(e){
                console.log(e)
            }
        })     
        });

       $("#btn_close").click(function(){
          window.location.href = "{{url('setting_list')}}";
       });

      });
    
        

  </script>
@endsection