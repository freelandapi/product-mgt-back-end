
@extends('Master.master')
@section('current_page_name', 'Setting');
@section('content')


 <!--================ Font Awesome================= -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!--================ Font Awesome================= -->
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- sweetalert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<style type="text/css">

    th,td{
      text-align: center;
    }
	/* show detail*/
		.product_view .modal-dialog{max-width: 1000px; width: 100%;}
        .pre-cost{text-decoration: line-through; color: #a5a5a5;}
        .space-ten{padding: 10px 0;}

        .checked {
		  color: orange;
		}

		div.red{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid red;
			  background-color: red;
			  border-radius: 100px;
		}

		div.blue{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid blue;
			  background-color: blue;
			  border-radius: 100px;
		}

		div.yellow{
			 display: inline-block;
			  width: 30px;
			  height: 30px;
			  padding: 5px;
			  border: 1px solid yellow;
			  background-color: yellow;
			  border-radius: 100px;
		}

</style>


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Setting</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">


                            <div class="row py-2" id="hide1">
                                    <div class="col-4">
                                    <a href="{{url('setting_add')}}" class="btn btn-sm btn-primary btn-content add_new" style="border-radius:20px"><i class="fa fa-fw fa-plus"></i>Add New</a>
                                    </div>
                                    <div class="col-8 my-auto mr-auto">
                                        <form class="form-inline mr-auto float-right">
                                            <input class="form-control mr-sm-2 enter_search" type="text" id="search" placeholder="Search Supplire" aria-label="Search Supplire">
                                            <button class="btn btn-cyan btn-rounded my-0"
                                                    id="txtUserSearh"
                                                    type="submit">
                                                <i class="fas fa-search btn_search"></i>
                                            </button>
                                        </form>
                                    </div>
                            </div>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Logo</th>
                                      <th>Website Name</th>
                                      <th>Address</th>
                                      <th>Phone</th>
                                      <th>Email</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="setting">

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                       


                    </div>

                    </div>

                </div>
            </section>


  <!-- ====== Modal Form  Show Detail ======  -->

   
    <!-- ==== End Modal Show ==== -->


    <script type="text/javascript">

       
        $(document).ready(function () {
            fetch_data();
        });

        function fetch_data() {
            $("#setting").html("");
            
            $.ajax({

                headers: HEADER,
                url: BASE_URL + "settings",
                method: "GET",
              
                success:function(res){
                //   console.log("setting: ",res.data)
                    var products = "";
                    $.each(res.data,function(index,val){

                        products = '<tr>'
                                        +'<td class="id">'+ val.id+'</td>'
                                        +'<td>'+ val.logo_image+'</td>'
                                        +'<td>'+ val.website_name+'</td>'
                                        +'<td>'+ val.address+'</td>'
                                        +'<td>'+ val.phone+'</td>'
                                        +'<td>'+ val.email+'</td>'

                                        +'<td>'
                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-info btn-sm btn_show" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-eye"></i></button>'

                                        + ' <button type="button"  style="border-radius: 20px;" class="btn btn-success btn-sm btn_edit" id="'+val.id+'" data-item="'+encodeURIComponent(JSON.stringify(val))+'"><i class="fa fa-fw fa-edit"></i></button>'

                                        + ' <button style="border-radius: 20px;" class="btn btn-danger btn-sm btn_delete"><i class="fa fa-fw fa-trash"></i></button>'
                                        +'</td>'
                                    + '</tr>';

                        $("#setting").append(products);
                    });
                
                },
                error: function(e){
                    console.log(e)
                }
            }); 
        }

    // ========== Edit  ========== //
             
    $("body").on("click",".btn_edit",function(){

        let id = $(this).attr("id");
        let data_item = $(this).attr("data-item");
        let data_edit = JSON.parse(decodeURIComponent(data_item));

        let website_name = data_edit.website_name;
        let website_url = data_edit.website_url;
        let address = data_edit.address;
        let phone = data_edit.phone ;
        let email = data_edit.email;
        let logo_image = data_edit.logo_image;
        let google_map = data_edit.google_map;
        let fb_link = data_edit.fb_link;
        let twitter_link = data_edit.twitter_link;
    
        let params = '?id='+id+'&website_name=' + website_name + '&website_url=' + website_url + '&address=' + address + '&phone=' + phone + '&email=' + email+ '&logo_image=' + logo_image+ '&google_map=' +google_map + '&fb_link=' + fb_link+ '&twitter_link='+ twitter_link;

        window.location.href = "{{url('setting_edit')}}" + params;

}); 

         //========= Event Delete with Sweetalert ===========//

         $("body").on('click', '.btn_delete', function(){

            var id = $(this).parents('tr').children('td.id').text();
            
            //  console.log("edit id:",id)
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(
                function(deleted)  {
                    if(deleted) {
                        delete_setting(id);
                    }else{
                        swal("Your file is safe!");
                    }
                });
            }); 

            function delete_setting(id){

            $.ajax({

                headers: HEADER,
                url: BASE_URL + 'settings/' + id,
                method: "DELETE",

                success:function(res){

                    $("#setting").html('');
                    fetch_data();
                },
                error: function(e){
                    console.log(e)
                }
            })
            }

    </script>
@endsection
