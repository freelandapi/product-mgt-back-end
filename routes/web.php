<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customAuth.login');
});

Route::get('dashboard', function (){
    return view('Master.initContent');
});
// PRODUCT CATEGORY
Route::get('pro_cate',function(){
   return view('Product_Management.Product_Category.product_category_list');
});

// RPODUCT
Route::get('products',function(){
    return view('Product_Management.Product.product_list');
});
Route::get('product_add',function(){
    return view('Product_Management.Product.product_add');
});

Route::get('product_edit',function(){
    return view('Product_Management.Product.product_edit');
});
Route::get('product_show',function(){
    return view('Product_Management.Product.product_show');
});


// SUPPLIRE
Route::get('supplire',function(){

    return view('Product_Management.Supplire.supplire_list');
});
Route::get('supplire_add',function(){

    return view('Product_Management.Supplire.supplire_add');
});
Route::get('supplire_edit',function(){

    return view('Product_Management.Supplire.supplire_edit');
});

// SETTING
Route::get('setting_list',function(){
    return view('Setting.setting_list');
});
Route::get('setting_add',function(){
    return view('Setting.setting_add');
});
Route::get('setting_edit',function(){
    return view('Setting.setting_edit');
});

// BRAND
Route::get('brands',function(){

    return view('Product_Management.Brand.brand_list');
});

// Role
Route::get('role',function(){
    return view('User_Management.Role.role_list');
});

// user
Route::get('user',function(){
    return view('User_Management.User.user_list');
});
Route::get('user_add',function(){
    return view('User_Management.User.user_create');
});
Route::get('user_edit',function(){
    return view('User_Management.User.user_edit');
});



//MEDIA FILES
Route::get('media_files', function () {
    return view('MediaFiles.list');
});
Route::get('media_files/new', function () {
    return view('MediaFiles.add_new');
});
Route::get('media_files/category', function () {
    return view('MediaFileCategory.list');
});
Route::get('media_files/category/create', function (){
    return view('MediaFileCategory.create');
});

//MENU MANAGEMENT
Route::get('menus/list', function () {
    return view('MenuView.list');
});
Route::get('menu/create', function () {
    return view('MenuView.create');
});
Route::get('menus/formModal', function () {
    return view('MenuView.formModal');
});


//PERMISSION 
Route::get('permissions', function() {
    return view('PermissionView.permission_list');
});


Route::get('index',function(){
   return view('index');
});



